# MoL Application

This is a RESTful application for managing work visa insurances.

## How to build and run

Just run with maven

```
mvn clean package spring-boot:run
```

## How to use

In order to access the protected resource, you must first request an access token via the OAuth. 
Request OAuth authorization:

```
curl -vu MoL_OAuth2_Client:123456 http://localhost:8081/mol/oauth/token -H "Accept: application/json" -d "&grant_type=password&username=ofs&password=Password@123"
```

A successful authorization results in the following JSON response:

```
{
  "access_token": "ff16372e-38a7-4e29-88c2-1fb92897f558",
  "token_type": "bearer",
  "expires_in": 359998,
  "scope": "read write"
}
```

Use the access_token returned in the previous request to make the authorized request to the protected endpoint:

```
curl http://localhost:8081/mol/api/users -H "Authorization: Bearer ff16372e-38a7-4e29-88c2-1fb92897f558"
```

If the request is successful, you will see the following JSON response:

```
[
    {
        "id": 1,
        "loginId": "beyadmin",
        "firstName": "Beyontec",
        "lastName": "Admin",
        "contactNumber": "9876543210",
        "emailId": "beyadmin@ofs.com",
        "createdAt": "2018-08-07T18:31:59.501",
        "roles": [
            {
                "id": 3,
                "name": "Approver"
            }
        ]
    }
]
```

After the specified time period, the access_token will expire. Use the refresh_token that was returned in the original OAuth authorization to retrieve a new access_token:

```
curl -vu MoL_OAuth2_Client:123456 http://localhost:8081/mol/oauth/token -H "Accept: application/json" -d "grant_type=refresh_token&refresh_token=f554d386-0b0a-461b-bdb2-292831cecd57"
```
