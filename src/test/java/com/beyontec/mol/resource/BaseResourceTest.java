package com.beyontec.mol.resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseResourceTest {

    private static final String ADMIN_USERNAME = "beyadmin";
    private static final String ADMIN_PASSWORD = "Password@123";
    private static final String EMPLOYEE_USERNAME = "beyuser";
    private static final String EMPLOYEE_PASSWORD = "Password@123";
    private static final String MOL_USERNAME = "moluser";
    private static final String MOL_PASSWORD = "Password@123";

    @Value("${security.oauth2.server.username}")
    private String clientId;

    @Value("${security.oauth2.server.password}")
    private String clientSecret;

	@Autowired
	protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    protected String getValidAccessToken(String username, String password) throws Exception {

        MockHttpServletResponse response = this.mockMvc
                .perform(post("/oauth/token")
                .header(HttpHeaders.AUTHORIZATION, 
                        "Basic " + Base64Utils.encodeToString((clientId + ":" + clientSecret).getBytes()))
                .param("username", username)
                .param("password", password)
                .param("grant_type", "password")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        return objectMapper.readValue(response.getContentAsByteArray(), OAuth2AccessToken.class).getValue();
    }

    protected HttpHeaders getAdminAuthHeaders() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + getValidAccessToken(ADMIN_USERNAME, ADMIN_PASSWORD));
        return headers;
    }

    protected HttpHeaders getEmployeeAuthHeaders() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + getValidAccessToken(EMPLOYEE_USERNAME, EMPLOYEE_PASSWORD));
        return headers;
    }

    protected HttpHeaders getMolAuthHeaders() throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + getValidAccessToken(MOL_USERNAME, MOL_PASSWORD));
        return headers;
    }
}
