package com.beyontec.mol.resource;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.beyontec.mol.entity.User;
import com.beyontec.mol.service.UserService;

public class UserResourceTest extends BaseResourceTest {

    @Autowired
    private UserService userService;

	@Test
	public void testGetAll() throws Exception {

		mockMvc.perform(get("/api/users")
				.headers(getEmployeeAuthHeaders())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].loginId", containsString("beyadmin")));
	}

    @Test
    public void testCreateUser() throws Exception {

        User user = new User();
        user.setLoginId("testuser");
        user.setFirstName("Test");
        user.setLastName("User");
        user.setEmailId("testuser@ofs.com");
        user.setContactNumber("1234567890");

        mockMvc.perform(post("/api/users")
                .headers(getAdminAuthHeaders())
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", containsString("Test")));
    }

    @Test
    public void testUpdateUser() throws Exception {

        User user = new User();
        user.setLoginId("testuser");
        user.setFirstName("Test");
        user.setLastName("User");
        user.setEmailId("testuser@ofs.com");
        user.setContactNumber("1234567890");
        user = userService.createUser(user);
        user.setFirstName("Test name modified");

        mockMvc.perform(put("/api/users/" + user.getId())
                .headers(getAdminAuthHeaders())
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", containsString("Test name modified")));
    }

    @Test
    public void testDeleteUser() throws Exception {

        User user = new User();
        user.setLoginId("testuserdelete");
        user.setFirstName("Test");
        user.setLastName("User to Delete");
        user.setEmailId("testuser@ofs.com");
        user.setContactNumber("1234567890");
        user = userService.createUser(user);

        mockMvc.perform(delete("/api/users/" + user.getId())
                .headers(getAdminAuthHeaders()))
                .andExpect(status().isOk());
    }
}
