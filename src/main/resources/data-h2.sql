INSERT INTO mol_user 
            (id, login_id, first_name, last_name, password, contact_number, email_id, created_at)
VALUES      (1, 'beyadmin', 'Beyontec', 'Admin', '$2a$08$nkkjD/nI1H8RjsfAPuLBJeWo8SYqkGMMlKUQZvGheCBTKoXFb88a6', 9876543210, 'beyadmin@ofs.com', SYSDATE()), 
            (2, 'beyuser', 'Beyontec', 'User', '$2a$08$nkkjD/nI1H8RjsfAPuLBJeWo8SYqkGMMlKUQZvGheCBTKoXFb88a6', 9876543220, 'beyuser@ofs.com', SYSDATE()),
            (3, 'moluser', 'MoL', 'User', '$2a$08$nkkjD/nI1H8RjsfAPuLBJeWo8SYqkGMMlKUQZvGheCBTKoXFb88a6', 9876543230, 'moluser@ofs.com', SYSDATE()); 

INSERT INTO mol_role 
            (id, name)
VALUES      (1, 'Admin'),
            (2, 'Employee'),
            (3, 'MoL');

INSERT INTO mol_acl 
            (id, name)
VALUES      (1, 'USER_GET_ALL'),
            (2, 'USER_MANAGE');

INSERT INTO mol_role_acl 
            (role_acl_id, role_id, acl_id)
VALUES      (1, 1, 1), 
            (2, 1, 2), 
            (3, 2, 1); 

INSERT INTO mol_user_role 
            (user_role_id, user_id, role_id)
VALUES      (1, 1, 1), 
            (2, 2, 2), 
            (3, 3, 3); 