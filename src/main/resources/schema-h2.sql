DROP TABLE IF EXISTS mol_user_role;
DROP TABLE IF EXISTS mol_role_acl;
DROP TABLE IF EXISTS mol_acl;
DROP TABLE IF EXISTS mol_role;
DROP TABLE IF EXISTS mol_user;

CREATE TABLE mol_user 
( 
    id             INT AUTO_INCREMENT PRIMARY KEY, 
    login_id       VARCHAR2(100), 
    first_name     VARCHAR2(50) NOT NULL, 
    last_name      VARCHAR2(50), 
    password       VARCHAR2(100) NOT NULL, 
    contact_number VARCHAR2(50), 
    email_id       VARCHAR2(100), 
    created_at     TIMESTAMP NOT NULL 
);

CREATE TABLE mol_role
( 
    id     INT AUTO_INCREMENT PRIMARY KEY, 
    name   VARCHAR2(50) NOT NULL
);

CREATE TABLE mol_acl
( 
    id     INT AUTO_INCREMENT PRIMARY KEY, 
    name   VARCHAR2(50) NOT NULL
);

CREATE TABLE mol_role_acl 
( 
    role_acl_id INT AUTO_INCREMENT PRIMARY KEY, 
    role_id INT NOT NULL, 
    acl_id INT NOT NULL, 
    FOREIGN KEY (role_id) REFERENCES mol_role(id), 
    FOREIGN KEY (acl_id) REFERENCES mol_acl(id)
);

CREATE TABLE mol_user_role 
( 
    user_role_id INT AUTO_INCREMENT PRIMARY KEY, 
    user_id INT NOT NULL, 
    role_id INT NOT NULL, 
    FOREIGN KEY (user_id) REFERENCES mol_user(id), 
    FOREIGN KEY (role_id) REFERENCES mol_role(id)
);
