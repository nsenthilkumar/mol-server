package com.beyontec.mol.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.beyontec.mol.entity.Role;
import com.beyontec.mol.entity.User;
import com.beyontec.mol.repository.UserRepository;

@Component
public class UserSecurityDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByLoginId(username);
        if (null == user) {
            throw new UsernameNotFoundException(String.format("Invalid Credentials: User %s does not exist", username));
        }
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (Role role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
            role.getAcls().stream().map(a -> new SimpleGrantedAuthority(a.getName())).forEach(authorities::add);
        }
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getLoginId(),
                user.getPassword(), authorities);
        return userDetails;
    }
}
