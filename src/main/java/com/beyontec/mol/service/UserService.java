package com.beyontec.mol.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.beyontec.mol.entity.User;
import com.beyontec.mol.exception.ApplicationException;
import com.beyontec.mol.exception.ErrorCode;
import com.beyontec.mol.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public User findUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public User findUserByLoginId(String loginId) {
        return userRepository.findByLoginId(loginId);
    }

    @Transactional
    public User createUser(User user) {

        RandomValueStringGenerator generator = new RandomValueStringGenerator(8);
        String password = generator.generate();
        user.setPassword(passwordEncoder.encode(password));
        return userRepository.save(user);
    }

    @Transactional
    public User updateUser(User user) {

        User rpUser = userRepository.findOne(user.getId());
        if (rpUser == null) {
            throw new ApplicationException(ErrorCode.ENTITY_NOT_FOUND);
        }
        rpUser.setFirstName(user.getFirstName());
        rpUser.setLastName(user.getFirstName());
        rpUser.setEmailId(user.getEmailId());
        rpUser.setContactNumber(user.getContactNumber());
        return userRepository.save(rpUser);
    }

    @Transactional
    public void deleteUser(Long id) {

        User rpUser = userRepository.findOne(id);
        if (rpUser == null) {
            throw new ApplicationException(ErrorCode.ENTITY_NOT_FOUND);
        }
        userRepository.delete(id);
    }
}
