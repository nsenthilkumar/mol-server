package com.beyontec.mol.config.security;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private ResourceServerTokenServices tokenServices;

    @Value("${security.jwt.resource-ids}")
    private String resourceIds;

    @Autowired
    private RouteAclConfig routeAclConfig;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceIds).tokenServices(tokenServices);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .requestMatchers()
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            .antMatchers("/oauth/token/**", "/auth/login/**", "/auth/logout/**", "/h2/**").permitAll()
            .antMatchers("/swagger-ui.html", "/api-docs/**").permitAll();

        Properties props = routeAclConfig.getProperties();
        props.forEach((key, value) -> {
            String[] url = key.toString().split("\\|");
            String[] acls = value.toString().split(",");
            try {
                if (url.length == 1) {
                    http.authorizeRequests().antMatchers(url[0]).hasAnyAuthority(acls);
                } else if (url.length == 2) {
                    http.authorizeRequests().antMatchers(HttpMethod.valueOf(url[1]), url[0]).hasAnyAuthority(acls);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        http
            .authorizeRequests()
            .anyRequest().authenticated()
            .and()
            .headers().frameOptions().disable();
    }
}
