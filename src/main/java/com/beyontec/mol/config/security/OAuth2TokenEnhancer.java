package com.beyontec.mol.config.security;

import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.beyontec.mol.entity.User;
import com.beyontec.mol.service.UserService;

import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

@Component
public class OAuth2TokenEnhancer implements TokenEnhancer {

    @Autowired
    private UserService userService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        User user = userService.findUserByLoginId(authentication.getName());

        Map<String, Object> additionalInfo = new HashMap<>();

        additionalInfo.put(OAuth2TokenKeys.USER_NAME.toString(), user.getLoginId());
        additionalInfo.put(OAuth2TokenKeys.FIRST_NAME.toString(), user.getFirstName());
        additionalInfo.put(OAuth2TokenKeys.LAST_NAME.toString(), user.getLastName());
        additionalInfo.put(OAuth2TokenKeys.CONTACT_NUMBER.toString(), user.getContactNumber());
        additionalInfo.put(OAuth2TokenKeys.EMAIL.toString(), user.getEmailId());

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
