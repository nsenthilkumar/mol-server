package com.beyontec.mol.config.security;

public enum OAuth2TokenKeys {

    USER_NAME("userName"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    CONTACT_NUMBER("contactNumber"),
    EMAIL("email");

    private final String val;

    private OAuth2TokenKeys(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return val;
    }
}
