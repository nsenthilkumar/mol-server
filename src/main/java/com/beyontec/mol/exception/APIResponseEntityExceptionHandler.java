package com.beyontec.mol.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class APIResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(APIResponseEntityExceptionHandler.class);

    @ExceptionHandler(ApplicationException.class)
    public final ResponseEntity<String> handleApplicationException(ApplicationException ex, WebRequest request) {

        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<String>(ex.getErrorCode().toString(), ex.getErrorCode().getHttpStatus());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public final ResponseEntity<String> handleDBConstraintViolationException(DataIntegrityViolationException ex,
            WebRequest request) {

        ErrorCode ec = ErrorCode.DATABASE_CONSTRAINT_VIOLATION;
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<String>(ec.toString(), ec.getHttpStatus());
    }

    @ExceptionHandler(CannotCreateTransactionException.class)
    public final ResponseEntity<String> handleDBAccessException(CannotCreateTransactionException ex,
            WebRequest request) {

        ErrorCode ec = ErrorCode.DATABASE_ACCESS_ERROR;
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<String>(ec.toString(), ec.getHttpStatus());
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<String> handleRuntimeException(RuntimeException ex, WebRequest request) {

        ErrorCode ec = ErrorCode.INTERNAL_SERVER_ERROR;
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<String>(ec.toString(), ec.getHttpStatus());
    }
}
