package com.beyontec.mol.exception;

public class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = -4371491428087549416L;

    private final ErrorCode code;

    public ApplicationException(ErrorCode code) {
        super();
        this.code = code;
    }

    public ApplicationException(String message, Throwable cause, ErrorCode code) {
        super(message, cause);
        this.code = code;
    }

    public ApplicationException(String message, ErrorCode code) {
        super(message);
        this.code = code;
    }

    public ApplicationException(Throwable cause, ErrorCode code) {
        super(cause);
        this.code = code;
    }

    public ErrorCode getErrorCode() {
        return this.code;
    }
}
