package com.beyontec.mol.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

    DATABASE_CONSTRAINT_VIOLATION(450, "DATABASE CONSTRAINT VIOLATION", HttpStatus.BAD_REQUEST),
    DATABASE_ACCESS_ERROR(451, "ERROR ACCESSING DATABASE", HttpStatus.INTERNAL_SERVER_ERROR),
    ENTITY_NOT_FOUND(452, "ENTITY NOT FOUND IN DATABASE", HttpStatus.BAD_REQUEST),
    OAUTH_SERVER_ERROR(453, "OAUTH AUTHENTICATION SERVER ERROR", HttpStatus.INTERNAL_SERVER_ERROR),
    INTERNAL_SERVER_ERROR(454, "SERVER ERROR : UNABLE TO PROCESS REQUEST", HttpStatus.INTERNAL_SERVER_ERROR),
    UNAUTHORIZED_ACCESS(452, "UNAUTHORIZED USER : UNABLE TO PROCESS REQUEST", HttpStatus.BAD_REQUEST);

    private final int code;
    private final String description;
    private final HttpStatus status;

    private ErrorCode(int code, String description, HttpStatus status) {
        this.code = code;
        this.description = description;
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Error Code:" + code + "  " + description;
    }
}
