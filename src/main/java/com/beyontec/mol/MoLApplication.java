package com.beyontec.mol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.beyontec.mol")
@EnableJpaRepositories("com.beyontec.mol.repository")
@EntityScan("com.beyontec.mol.entity")
public class MoLApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoLApplication.class, args);
    }
}