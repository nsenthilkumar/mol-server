package com.beyontec.mol.resource;

import java.util.stream.Collectors;

import org.springframework.validation.Errors;

public abstract class BaseResource {

    protected String getErrorMessage(Errors errors) {
        return errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
    }
}
